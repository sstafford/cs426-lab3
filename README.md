# README #

This repository contains the work I did for Daniel Defreez during the winter of 2012 for his Linux Systems Administration class at Southern Oregon University Computer Science. Here is Daniel's problem statement:

CS 426: Homework 3

A Better Apache Module
Background

We will extend the Puppet module that you built in the previous lab to
include some Apache-specific configuration options. Particularly, your
module will support defining multiple virtual hosts and enabling SSL.

If you are interested in seeing a full-fledged Apache module, look at
https://github.com/puppetlabs/puppetlabs-apache. This is the official
Apache module distributed by Puppet Labs. It is /much/ more complicated
than yours needs to be, but it may give you some ideas if you get stuck.


 Instructions

 1. Deploy a VM with Vagrant and your Apache module.
 2. By hand, globally disable the ServerSignature and set ServerTokens
    to Prod.
 3. Now do this as part of your Puppet module.
 4. By hand, configure the VM to support two name-based virtual hosts:
    example.com and example2.com.
 5. You can test your virtual host configuration by setting DNS entries
    in the hosts file on the host machine (/etc/hosts on Linux,
    %systemroot%\system32\drivers\etc\hosts on Windows).
 6. Extend your Apache module with a defined resource type that creates
    name-based virtual hosts. The vhost specifics should not be defined
    inside the module. That is, at least the ServerName and DocumentRoot
    must be passed as parameters.
 7. By hand, configure one of the vhosts to support SSL with a
    self-signed certificate that matches the DNS name. When SSL is
    enabled on a vhost, all http traffic should be redirected or
    rewritten to https.
 8. Fold the SSL configuration into your Puppet module.
 9. Modify your vagrant project to apply the module. You should have a
    manifest outside of the module that uses your defined resource type
    to create the vhosts. Enabled SSL on the example.com vhost.
10. Test your work (destroy the VM and vagrant up from scratch).


    Deliverables

 1. Create a Google Doc with instructions for manually configuring
    name-based virtual hosts, enabling SSL on a particular vhost, and
    redirecting http to https.
 2. Share the document with me and submit the share link on Moodle.
 3. Create a BitBucket repository called cs426-lab3.
 4. Push your finished Vagrant project - including the puppet module(s)
    - to Bitbucket.
 5. Grant read access to defreez_sou.


    Requirements

  * Your vhost for example.com must listen on 80 and 443, redirecting
    traffic from http to https.
  * The certificate for the example.com vhost must use the DNS name
    example.com.
  * You must enable another vhost for example2.com that does not have
    SSL enabled.
  * I will clone your repo and run vagrant up. I should get a working
    web server with the vhosts correctly configured.
  * You must use a definition in your Puppet module to configure the vhosts.
  * You must be able to define any number of vhosts using the module,
    and enable SSL on any number of them. You may assume that the client
    browser supports SNI.