include apache

apache::vhost { 'secure.example2':
	vhostname => '*',
	servername => 'secure.example2.com',
    docroot => '/var/www/example2',
	dest     => '',
	template => 'apache/vhost-ssl.conf.erb',
}
apache::vhost { 'example':
	 vhostname =>  '*',
	 servername => 'example.com',
     docroot => '/var/www/example',
	 dest    => '',
	 template   => 'apache/vhost-default.conf.erb',
}
apache::vhost  { 'example2' :
	 vhostname =>  '*',
	 servername => 'example2.com',
     docroot => '',
	 dest     => ' https://secure.example2.com',
	 template => 'apache/vhost-redirect.conf.erb',
}
	

