define apache::vhost ($vhostname, $docroot, $servername, $template, $dest) {
  file { "$servername":
        content => template("$template"), 
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        path    => "/etc/httpd/conf.d/${name}.conf",
        require => Package['httpd'],
        notify  => Service['httpd'],
  }
}
