class apache
{
    package { 'httpd':
        ensure  => 'installed',
        before => Service['httpd'],
	}
    service {'httpd':
		ensure => 'running',
		enable => 'true',
		subscribe => File['/etc/httpd/conf/httpd.conf'],
    }
    package { 'mod_ssl':
		ensure => 'installed',
        require => Package['httpd'],
    }
    file{ 'ssl.conf':
		ensure => 'file',
		path   => '/etc/httpd/conf.d/ssl.conf',
		owner  => 'root',	  		
		group  => 'root',
		mode   => '0644',
	    source => 'puppet:///modules/apache/ssl.conf',
		require => Package['mod_ssl'],
	}
    file { 'key':
		ensure => 'file',
		path   => '/etc/pki/tls/private/ca.key',
		owner  => 'root',
		group  => 'root',
		mode   => '0644',
	    source => 'puppet:///modules/apache/ca.key',
		require => Package['mod_ssl'],
	}	
    file { 'csr':
		ensure => 'file',
		path   => '/etc/pki/tls/private/ca.csr',
		owner  => 'root',	
		group =>  'root',
        mode  =>  '0644',
	    source => 'puppet:///modules/apache/ca.csr',
		require => Package['mod_ssl'],
    }
    file { 'crt':
		ensure => 'file',
		path   => '/etc/pki/tls/certs/ca.crt',
		owner  => 'root',	
		group  => 'root',
        mode  =>  '0644',
	    source => 'puppet:///modules/apache/ca.crt',
		require => Package['mod_ssl'],
	}
    file { 'httpd.conf':
		ensure => 'file',
		path  =>  '/etc/httpd/conf/httpd.conf',
		owner =>  'root',
		group =>  'root',
    	mode  =>  '0644',
		source => 'puppet:///modules/apache/httpd.conf',
		require =>  Package['httpd'],
    }
	file {'example' :
          ensure => 'directory',
          path => '/var/www/example',
          owner => 'root',
          group => 'root',
          mode =>  '0644',
          require => Package['httpd'],
    }
    file { 'index':
          ensure => 'file',
          path =>   '/var/www/example/index.html',
          owner =>  'root',
          group =>  'root',
          mode =>   '0777',
          source => 'puppet:///modules/apache/index.html',
          require => File['/var/www/example'],
    }
	file { 'example2':
          ensure => 'directory',
          path =>   '/var/www/example2',
          owner =>  'root',
          group =>  'root',
          mode =>   '0644',
          require => Package['httpd'],
	}
	file { 'index2':
        ensure => 'file',
        path =>   '/var/www/example2/index.html',
        owner =>  'root',
        group =>  'root',
        mode =>   '0644',
        source => 'puppet:///modules/apache/index2.html',
        require => File['/var/www/example2'],
    }
    file { 'iptables':
        path    => '/etc/sysconfig/iptables',
	    owner   => 'root',
	    group   => 'root',
	    mode	=> '0600',
	    source	=> 'puppet:///modules/apache/iptables',
    }
    exec { 'restore':
    	command =>  '/sbin/iptables-restore < /etc/sysconfig/iptables',
        subscribe => File['/etc/sysconfig/iptables'],
        refreshonly => 'true',
    }
 
}
