class apache
{
    package { 'httpd':
        ensure  => 'installed',
    }

    service { 'httpd':
        ensure  => 'running',
        enable  => 'true',
       # require => File['/etc/httpd/conf/httpd.conf'],
    }
    file { 'iptables':
        path    => '/etc/sysconfig/iptables',
	owner   => 'root',
	group   => 'root',
	mode	=> '0600',
	source	=> 'puppet:///modules/apache/iptables',
    }
    

 	
    file { '/sbin/iptables-restore':
  	ensure => 'link',
  	target => '/sbin/iptables-multi',
     }

    exec { 'restore':
	user => 'root',
  	command =>  '/sbin/iptables-restore < /etc/sysconfig/iptables',
     #   require => File['/etc/sysconfig/iptables'],
    }

}
